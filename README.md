# JavaScript Clock

## Description
This project utilizes JavaScript to get the current date in your local timezone and display an animated analog clock on the screen.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Technologies
* HTML for the webpage
* CSS for the styling
* JavaScript to get the current time and animate the clock

## Contributing
If anyone would like to contribute to the project, feel free to pull the code from [GitLab](https://gitlab.com/jhawkes_portfolio/clock) and create a merge request for review

## Authors
Jordan Hawkes

## Source
This project was inspired by Ania Kubow and the following [YouTube tutorial](https://www.youtube.com/watch?v=LNL1otheWeY&t=257s).

## MIT Licence
Copyright (c) 2024 Jordan Hawkes

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.